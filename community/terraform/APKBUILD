# Contributor: Thomas Boerger <thomas@webhippie.de>
# Contributor: Gennady Feldman <gena01@gmail.com>
# Contributor: Sergii Sadovyi <serg.sadovoi@gmail.com>
# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Thomas Boerger <thomas@webhippie.de>
pkgname=terraform
pkgver=1.1.8
pkgrel=1
pkgdesc="Building, changing and combining infrastructure safely and efficiently"
url="https://www.terraform.io/"
arch="all"
license="MPL-2.0"
makedepends="go"
checkdepends="openssh-client"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/terraform/archive/v$pkgver.tar.gz"
builddir="$srcdir/src/github.com/hashicorp/$pkgname"
options="chmod-clean"

prepare() {
	mkdir -p ${builddir%/*}
	mv $srcdir/$pkgname-$pkgver "$builddir"/
	default_prepare
}

export GOPATH="$srcdir"

build() {
	go build -v -o bin/$pkgname \
		-mod=readonly -ldflags "-X main.GitCommit=v$pkgver -X github.com/hashicorp/terraform/version.Prerelease= -s -w"
}

check() {
	case "$CARCH" in
		arm*|x86)
			go list -mod=readonly . | xargs -t -n4 \
			go test -mod=readonly -timeout=2m -parallel=4
			;;
		*) go test ./... ;;
	esac
	bin/$pkgname -v
}

package() {
	install -Dm755 "$builddir"/bin/$pkgname "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
0e014373e62eb8f23e60fe94279cf266c2bd3cbc685d79432862fe371627fb5358314ae9e4c39988b8044413e63d85e0b844f0345a709c64cee33ce63448c2cc  terraform-1.1.8.tar.gz
"
