# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gnome-authenticator
pkgver=4.0.3
pkgrel=1
pkgdesc="Two-Factor Authentication application"
url="https://gitlab.gnome.org/World/Authenticator"
# s390x, riscv64 blocked by libadwaita
# x86, ppc64le building dependency ring v0.16.20 fails
arch="all !s390x !riscv64 !x86 !ppc64le"
license="GPL-3.0-only"
makedepends="meson cargo gstreamer-dev gst-plugins-bad-dev gst-plugins-base-dev gtk4.0-dev libadwaita-dev zbar-dev"
checkdepends="appstream-glib desktop-file-utils"
subpackages="$pkgname-dbg $pkgname-lang"
source="https://gitlab.gnome.org/World/Authenticator/-/archive/$pkgver/Authenticator-$pkgver.tar.gz
	meson-merge-file-remove-positional-args.patch"
builddir="$srcdir/Authenticator-$pkgver"

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild -v -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
cfd5d1a51de0f5b943a70949b354c1af01db05f3523cfcbb700352789c1dd85e13dd57c390b159b640247bf96e0107d01653464715e865f45724127f8a2458ce  Authenticator-4.0.3.tar.gz
1a79e8662335e19d500d3dba72a9e0723a2ffca54050d745adf659ceda0ac53fd8ffe3b28f126c36c470decffad7bf2b4c75394be4f23543f2d232e6a03646c5  meson-merge-file-remove-positional-args.patch
"
